# EPAM 107 1 Animation Importing and Subeditors​

Developed with Unreal Engine 5

## Task Description: 

This task will give a brief overview of the Import Animation function in Unreal as well as Animation related Subeditors (Skeleton, Skeletal Mesh, and Animation Editors). 

A project should be created from the Third Person Template with the Starter Pack! 

## List of assets that should be present in the Project’s Content Folder: 

1. A map with the animation and the socket actor 
2. FBX file for existing exported Third Person Animation 
3. Imported Files: 
--Skeletal Mesh 
--Physics Asset 
--Skeleton 
--M_UE4Man_Body Material 
--M_UE4Man_ChestLogo 
4. Static Mesh of the baked animation frame 
5. Skeleton for the UE5 Mannequin with Head Socket 
6. Custom Animation Sequence 

## Requirements for the assets: 

Animation should be imported into Unreal with the following options in the Import FBX dialog: 

--Skeletal Mesh - Enabled 
--Import Mesh – Enabled 
--Skeleton – None 
--Import Animation – Enabled 
--Animation Length – Exported Time 

Baked Animation: 

--The pose of the Mannequin should be unique! 

Skeleton: 

--Head bone has a Socket 
--Socket is located on top of the head of the Mannequin Skeletal Mesh 

Animation Sequence: 

--Animation should be unique! 

Map: 

--Any map will do 
--An Animation with the Head Socket should be present on the level 
--Any object of choice should be attached to the Head Socket